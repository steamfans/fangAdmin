export default {
  install (Vue, options) {
    Vue.prototype.token = function () {
      let token = this.$cookies.get('token') || '';
      return token;
    };
    Vue.prototype.getUrl = function () {
      let url = location.search; // 获取url中"?"符后的字串
      let theRequest = {};
      if (url.indexOf('?') !== -1) {
        let str = url.substr(1);
        let strs = str.split('&');
        for (let i = 0; i < strs.length; i++) {
          theRequest[strs[i].split('=')[0]] = unescape(strs[i].split('=')[1]);
        }
      }
      return theRequest;
    };
    Vue.prototype.formatterTime = function (value) {
      if (!value) {
        return '';
      }
      let time = new Date(value);
      let year = time.getFullYear();
      let month = time.getMonth() + 1;
      if (month < 10) {
        month = '0' + month;
      }
      var date = time.getDate();
      if (date < 10) {
        date = '0' + date;
      }
      var hours = time.getHours();
      if (hours < 10) {
        hours = '0' + hours;
      }
      var min = time.getMinutes();
      if (min < 10) {
        min = '0' + min;
      }
      var sec = time.getSeconds();
      if (sec < 10) {
        sec = '0' + sec;
      }
      return year + '-' + month + '-' + date + ' ' + hours + ':' + min + ':' + sec;
    };
    Vue.prototype.formatterDate = function (value) {
      if (!value) {
        return '';
      }
      let time = new Date(value);
      let year = time.getFullYear();
      let month = time.getMonth() + 1;
      if (month < 10) {
        month = '0' + month;
      }
      var date = time.getDate();
      if (date < 10) {
        date = '0' + date;
      }
      return year + '-' + month + '-' + date;
    };
    Vue.filter('filterDate', function (value) {
      if (!value) {
        return '-';
      }
      let time = new Date(value);
      let year = time.getFullYear();
      let month = time.getMonth() + 1;
      if (month < 10) {
        month = '0' + month;
      }
      var date = time.getDate();
      if (date < 10) {
        date = '0' + date;
      }
      return year + '-' + month + '-' + date;
    });
    Vue.filter('filterTime', function (value) {
      if (!value) {
        return '-';
      }
      let time = new Date(value);
      let year = time.getFullYear();
      let month = time.getMonth() + 1;
      if (month < 10) {
        month = '0' + month;
      }
      var date = time.getDate();
      if (date < 10) {
        date = '0' + date;
      }
      var hours = time.getHours();
      if (hours < 10) {
        hours = '0' + hours;
      }
      var min = time.getMinutes();
      if (min < 10) {
        min = '0' + min;
      }
      var sec = time.getSeconds();
      if (sec < 10) {
        sec = '0' + sec;
      }
      return year + '-' + month + '-' + date + ' ' + hours + ':' + min + ':' + sec;
    });
    Vue.filter('filterMoney', function (value) {
      if (!value && value != 0) {
        return '-';
      } else {

      }
    });
  }
};
