import main from '@/pages/main/main';

const staticRoute = [{
  path: '/',
  redirect: '/home'
},
{
  path: '/error',
  component: () =>
      import(/* webpackChunkName: 'error' */ '@/pages/error'),
  children: [{
    path: '401',
    component: () =>
          import(/* webpackChunkName: 'error' */ '@/pages/error/401')
  },
  {
    path: '403',
    component: () =>
          import(/* webpackChunkName: 'error' */ '@/pages/error/403')
  },
  {
    path: '404',
    component: () =>
          import(/* webpackChunkName: 'error' */ '@/pages/error/404')
  },
  {
    path: '500',
    component: () =>
          import(/* webpackChunkName: 'error' */ '@/pages/error/500')
  }
  ]
},
{
  path: '/home',
  component: main,
  children: [{
    path: '',
    component: () =>
        import(/* webpackChunkName: 'main' */ '@/pages/home/home')
  }]
},
{
  path: '/sys',
  component: main,
  children: [{
    path: 'user',
    component: () =>
        import(/* webpackChunkName: 'main' */ '@/pages/sys/sys_user')
  }, {
    path: 'menu',
    component: () =>
        import(/* webpackChunkName: 'main' */ '@/pages/sys/sys_menu')
  }]
},
{
  path: '/features',
  component: main,
  children: [{
    path: 'todayoil',
    component: () =>
        import(/* webpackChunkName: 'main' */ '@/pages/features/todayoil')
  },
  {
    path: 'weather',
    component: () =>
        import(/* webpackChunkName: 'main' */ '@/pages/features/weather')
  }]
},
{
  path: '/components',
  component: main,
  children: [{
    path: '',
    component: () =>
          import(/* webpackChunkName: 'main' */ '@/pages/components/components')
  },
  {
    path: 'permission',
    component: () =>
          import(/* webpackChunkName: 'main' */ '@/pages/components/1')
  }
  ]
},
{
  path: '/login',
  component: () =>
      import(/* webpackChunkName: 'login' */ '@/pages/login/login')
},
{
  path: '*',
  redirect: '/home'
}];

export default staticRoute;
