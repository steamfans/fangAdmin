import * as types from './metation-types';

const mutations = {
  [types.SET_LANG] (state, str) {
    state.lang = str;
  }
};

export default mutations;
