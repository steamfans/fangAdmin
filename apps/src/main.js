// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '../mock/index.js';

import Vue from 'vue';
import App from './App';
import router from './router';
import VueCookies from 'vue-cookies';
import store from './store';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import './common/element-ui.styl';
import './common/mixin.styl';

// 引用全局的配置文件
import config from './config/config.js';

// 引用API文件
import api from './api/index.js';
// 将API方法绑定到全局
Vue.prototype.$api = api;

// 将全局变量绑定到vue
Vue.use(VueCookies);
Vue.use(config);
Vue.use(ElementUI);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
