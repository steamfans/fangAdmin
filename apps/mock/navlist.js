var data = [
  {
    path: '/home',
    name: '首页'
  },
  {
    name: '系统管理',
    child: [
      {
        name: '用户管理',
        path: '/sys/user'
      },
      {
        name: '菜单管理',
        path: '/sys/menu'
      },
      {
        name: '功能类',
        child: [
          {
            path: '/components/permission',
            name: '详细鉴权'
          }
        ]
      }
    ]
  },
  {
    name: '功能管理',
    child: [
      {
        name: '今日油价',
        path: '/features/todayoil'
      },
      {
        name: '天气使用记录',
        path: '/features/weather'
      }
    ]
  }
];

export default [{
  path: '/user/navlist',
  data: data
}];
