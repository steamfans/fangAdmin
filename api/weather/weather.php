<?php
  $host = "https://ali-weather.showapi.com";
  $path = "/area-to-weather";
  $method = "GET";
  $appcode = "17691e95433548018a5559b338b4b2ab";
  $headers = array();
  array_push($headers, "Authorization:APPCODE " . $appcode);
  $querys = "area=北京&need3HourForcast=0&needAlarm=0&needHourData=0&needIndex=0&needMoreDay=1";
  $bodys = "";
  $url = $host . $path . "?" . $querys;

  $curl = curl_init();
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_FAILONERROR, false);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HEADER, false);
  if (1 == strpos("$".$host, "https://"))
  {
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
  }
  // echo (curl_exec($curl));
  $data = json_decode(urldecode(curl_exec($curl)));
  $result = $data->showapi_res_body;//获得result数组

  $jobj=new stdclass();//实例化stdclass，这是php内置的空类，可以用来传递数据，由于json_encode后的数据是以对象数组的形式存放的，
  $jobj->success=true;
  $jobj->data=$result;

  echo json_encode($jobj);

?>